import { Chapter } from './chapter';
export class Book {
    ID:string = "";
    Name: string = "";
    Chapter: Array<Chapter> = null;
}

package controllers

const (
	// SessionAutoLogin 是否 自動 使用 cookie 登入
	SessionAutoLogin = "__AutoLogin"

	// Enabled 啟用
	Enabled = "1"
	// Disabled 關閉
	Disabled = "0"

	// True .
	True = "1"
	// False .
	False = "0"
)
